# nfse-scraper

- nfse_campinas

## Preparando o ambiente
- PhantomJS
- Tesseract

### Linux
```sh
#abre o terminal e instale virtualenv e use o comando pra criar a pasta
$ virtualenv -p python3 nfse
$ cd nfse && source bin/activate
$ git clone https://gitlab.com/erickson_douglas/nfse-scraper.git
$ cd nfse-scraper
$ pip install scrapy selenium pytesseract scrapyrt pillow
```

## Endpoint API

#### Adicionar as credencias dentro do spider nfse_campinas para o GET
![](https://gitlab.com/erickson_douglas/nfse-scraper/raw/master/imagens/credencial.png)
### Ative o endpoint API no terminal

``` sh
$ scrapyrt -p 3000
```

### Em outro terminal, use o comando para GET ou POST

#### GET

``` sh
$ curl 'http://localhost:3000/crawl.json?start_requests=true&spider_name=nfse_campinas'

```
#### Ou pelo Navegador, use a estensão para facilitar na visualização do json
- Firefox [Json-handle](https://addons.mozilla.org/pt-BR/firefox/addon/json-handle/?src=search)
- Chrome  [Json-formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa?hl=pt-BR)


#### POST

``` sh
$ curl -XPOST -d '{"spider_name":"nfse_campinas","start_requests": false,  \
"request":{"url":"https://nfse.campinas.sp.gov.br/NotaFiscal/index.php/","method":"POST",  \ 
"cb_kwargs":{"credencial":{"cnpj":"<CNPJ>","nfse":"<NFSE>","inscricao": "<INSCRICAO>","cod_verificacao": "<COD_VERIFICACAO>"}}}}'  \ 
"http://localhost:3000/crawl.json"
```

Futuras melhorias:
- Treinar o Tensorflow para quebrar mais rapido o CAPTCHA
- Alterar o PhantomJS para Splash
- Criar uma função para verificar se as credenciais estão corretos, caso ao contrario, retornar o erro.
