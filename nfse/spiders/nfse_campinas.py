# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from selenium import webdriver
from time import sleep
import urllib.request
from pytesseract import image_to_string
from PIL import Image



class NfseCampinasSpider(scrapy.Spider):
    name = 'nfse_campinas'
    #allowed_domains = ['https://nfse.campinas.sp.gov.br/NotaFiscal/index.php']
    start_urls = ['https://nfse.campinas.sp.gov.br/NotaFiscal/index.php/']

    def credencial(self):
        return {'cnpj':'',
                'nfse':'',
                'inscricao':'',
                'cod_verificacao': ''}

    def start_requests(self):
        #for url in self.start_urls: yield SplashRequest(url=url,callback=self.parse,endpoint='render.json')
        yield  scrapy.Request(url=self.start_urls[0],callback=self.parse,cb_kwargs=dict(credencial=self.credencial()))

    def parse(self,response,credencial):
        html = self.browser_selenium(infor=credencial)
        #print(html)
        if html:
            yield {'CABECALHO':self.cabecalho(html.css('table.impressaoTabela')[0]),
                   'INFORMACOES_NFSE':self.informacoes_nfse(html.css('table.impressaoTabela')[1]),
                   'PRESTADOR_DE_SERVICOS':self.prestacao_servico(html.css('table.impressaoTabela')[2]),
                   'TOMADOR_DE_SERVICOS':self.prestacao_servico(html.css('table.impressaoTabela')[3]),
                   "DISCRIMINACAO_DOS_SERVICOS":self.discriminacao_dos_servicos(html.css('table.impressaoTabela')[4]),
                   "OUTRAS_INFORMACOES":self.outras_informacoes(html.css('table.impressaoTabela')[8])}

        else:
            yield {'Erro':html}

    def cabecalho(self,html):
        result= dict()
        result['cabecalho'] = html.css('td.impressaoCabecalho::text').extract_first().strip()
        result['titulo']    = html.css('td.impressaoTitulo::text').extract_first().strip()
        return result

    def informacoes_nfse(self,html):
        result= dict()
        labels =html.css('td.impressaoLabel ::text').extract()
        campos =html.css('td.impressaoTitulo ::text').extract()
        for label,campo in zip(labels,campos):
            result[label] = campo
        return result

    def prestacao_servico(self,html):
        result = dict()
        for tr in html.css("tr")[1:]:
            label = tr.css("td.impressaoLabel::text").extract_first().replace(':','').strip()
            campo = tr.css("span.impressaoCampo::text").extract_first().replace(':','').strip()
            result[label] = campo
        return result

    def discriminacao_dos_servicos(self,html):
        result = {'Tributavel':list(),'Item':list(),'Qtde':list(),'Unitario':list(),'Total':list()}
        campos = html.css('tr.impressaoCampo')
        for line in campos:
            campo = line.css('td::text').extract()
            result['Tributavel'].append(campo[0].strip())
            result['Item'].append(campo[1].strip())
            result['Qtde'].append(campo[2].strip())
            result['Unitario'].append(campo[3].strip())
            result['Total'].append(campo[4].strip())
        return result

    def outras_informacoes(self,html):
        result = dict()
        for line in html.css('td.impressaoLabel::text').extract():
            if ':' in line:
                label,campo = line.split(":")
                result[label.strip()] = campo.strip()
            elif line != " ": result[label.strip()]+=" "+line.strip()
        return result


    def browser_selenium(self,infor):
        browser = webdriver.PhantomJS()
        #browser = webdriver.Firefox()
        url_verificar = 'https://nfse.campinas.sp.gov.br/NotaFiscal/verificarAutenticidade.php'

        def entrar_site():
            count = 0
            while count<5:
                try:
                    browser.get(self.start_urls[0])
                    sleep(2)
                    browser.find_element_by_css_selector('#principal').click()
                    browser.get(url_verificar)
                    sleep(2)
                    break
                except:
                    sleep(2)
                    count+1
            else: return None

        def inserir_infor():
            browser.find_element_by_css_selector('#rPrest').send_keys(infor['cnpj'])
            browser.find_element_by_css_selector('#rNumNota').send_keys(infor['nfse'])
            browser.find_element_by_css_selector('#rInsMun').send_keys(infor['inscricao'])
            browser.find_element_by_css_selector('#rCodigoVerificacao').send_keys(infor['cod_verificacao'])
        def quebrar_captcha():
            url_img = browser.find_element_by_css_selector('tbody tr td img').get_attribute("src")
            urllib.request.urlretrieve(url_img, "captcha.png")
            infor['captcha'] = image_to_string(Image.open('captcha.png'),config=r'--oem 2 --psm 13 --dpi 70',lang='por')
            browser.find_element_by_name('rSelo').send_keys(infor['captcha'][:4])
            browser.find_element_by_css_selector('.botao').click()
            sleep(2)
            if len(browser.window_handles)>2:
                for line in browser.window_handles[:-1]:
                    browser.switch_to_window(line)
                    browser.close()
                browser.switch_to_window(browser.window_handles[0])
            else:
                if len(browser.window_handles)==2:
                    browser.switch_to_window(browser.window_handles[-1])
                    browser.close()
                    browser.switch_to_window(browser.window_handles[0])
                browser.refresh()
                sleep(3)
                quebrar_captcha()
                #if "Imagem" in browser.find_element_by_css_selector("td.titErro").text: quebrar_captcha()
                #else: error = browser.find_element_by_css_selector("td.titErro").text

        try:
            entrar_site()
            inserir_infor()
            quebrar_captcha()
            sleep(5)

            html = Selector(text=browser.page_source)
            browser.quit()
            print("passsou --->")
            return html
        except Exception as e:
            print(e)
            browser.quit()








        html = browser.page_source
        browser.close()
        return html
